import React from 'react'
import { useFormContext } from './contexts/FormContext'
import useStyles from './styles/FormInput.styles'

export default function FormOutput() {
  const { output } = useFormContext()
  const classes = useStyles()

  return (
    <label htmlFor="decimal-value" className={classes.label}>
      Valor decimal:
      <input
        alt="Valor decimal"
        id="decimal-value"
        name="decimal-value"
        value={output}
        disabled
      />
    </label>
  )
}

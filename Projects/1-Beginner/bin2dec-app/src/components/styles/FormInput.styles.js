import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme =>({
  label: {
    height: '50%',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    color: 'white',
    fontWeight: '500',
    fontSize: '1.6rem',
    textShadow: '4px 4px 8px black',

    [theme.breakpoints.up('xs')]: {
      fontSize: '1rem',
    },

    [theme.breakpoints.up('sm')]: {
      fontSize: '1.6rem',
    },

    '& > input': {
      height: '30%',
      width: '50%',
      borderRadius: '40px',
      border: 'none',
      outline: 'none',
      padding: '0 20px'
    }
  }
}))

export default useStyles

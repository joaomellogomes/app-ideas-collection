import React from 'react'
import { useFormContext } from './contexts/FormContext'
import { useFormInputController } from './ViewController/FormInputController'
import useStyles from './styles/FormInput.styles'

export default function FormInput() {
  const { textInput } = useFormContext()
  const { handleChange } = useFormInputController()
  const classes = useStyles()

  return (
    <label htmlFor="input-binary-value" className={classes.label}>
      Número binário:
      <input
        alt="Inserir número binário"
        id="input-binary-value"
        name="input-binary-value"
        type="text"
        // maxLength={8}
        value={textInput}
        onChange={handleChange}
      />
    </label>
  )
}
